﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class VectorExstensions
{

    public static Vector2 GetIntersectionPointCoordinates(Vector2 A1, Vector2 A2, Vector2 B1, Vector2 B2)//, out bool found)
    {
        bool found;
        float tmp = (B2.x - B1.x) * (A2.y - A1.y) - (B2.y - B1.y) * (A2.x - A1.x);

        if (tmp == 0)
        {
            // No solution!
            found = false;
            return Vector2.zero;
        }

        float mu = ((A1.x - B1.x) * (A2.y - A1.y) - (A1.y - B1.y) * (A2.x - A1.x)) / tmp;

        found = true;

        return new Vector2(
            B1.x + (B2.x - B1.x) * mu,
            B1.y + (B2.y - B1.y) * mu
        );
    }

    public static Vector3 GetIntersectionPointCoordinates3DSpace(Vector3 A1, Vector3 A2, Vector3 B1, Vector3 B2)//, out bool found)
    {
        bool found;
        float tmp = (B2.x - B1.x) * (A2.z - A1.z) - (B2.z - B1.z) * (A2.x - A1.x);

        if (tmp == 0)
        {
            // No solution!
            found = false;
            return Vector2.zero;
        }

        float mu = ((A1.x - B1.x) * (A2.z - A1.z) - (A1.z - B1.z) * (A2.x - A1.x)) / tmp;

        Vector3 intersectPoint = new Vector3(
            B1.x + (B2.x - B1.x) * mu,
            0,
            B1.z + (B2.z - B1.z) * mu
        );

        found = true;

        return intersectPoint;
    }

    public static Vector3 GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(Vector3 A1, Vector3 A2, Vector3 B1, Vector3 B2)//, out bool found)
    {
        Vector3 intersectPoint = GetIntersectionPointCoordinates3DSpace(A1, A2, B1, B2);

        if (intersectPoint != Vector3.zero && IsBetween(A1, A2, intersectPoint) && IsBetween(B1, B2, intersectPoint))
        {
            return intersectPoint;
        }
        else
        {
            return Vector3.zero;
        }
    }

    //linePoint - point the line passes through
    //lineDirection - unit vector in direction of line, either direction works
    //sourcePoint - the point to find nearest on line for
    public static Vector3 NearestPointOnLine(Vector3 linePoint, Vector3 lineDirection, Vector3 sourcePoint)
    {
        lineDirection.Normalize();//this needs to be a unit vector
        var v = sourcePoint - linePoint;
        var d = Vector3.Dot(v, lineDirection);
        return linePoint + lineDirection * d;
    }

    public static Vector3 NearestPointOnLine(Line line, Vector3 sourcePoint)
    {
        return NearestPointOnLine(line.A1, line.A2 - line.A1, sourcePoint);
    }

    //Is a point c between 2 other points a and b?
    static bool IsBetween(Vector2 a, Vector2 b, Vector2 c)
    {
        bool isBetween = false;

        //Entire line segment
        Vector2 ab = b - a;
        //The intersection and the first point
        Vector2 ac = c - a;

        //Need to check 2 things: 
        //1. If the vectors are pointing in the same direction = if the dot product is positive
        //2. If the length of the vector between the intersection and the first point is smaller than the entire line
        if (Vector2.Dot(ab, ac) > 0f && ab.sqrMagnitude >= ac.sqrMagnitude)
        {
            isBetween = true;
        }

        return isBetween;
    }

    static bool IsBetween2(Vector2 a, Vector2 b, Vector2 c)
    {
        bool isBetween = false;

        //Entire line segment
        Vector2 bc = b - c;
        //The intersection and the first point
        Vector2 ac = c - a;

        //Need to check 2 things: 
        //1. If the vectors are pointing in the same direction = if the dot product is positive
        //2. If the length of the vector between the intersection and the first point is smaller than the entire line
        if (Vector2.Dot(bc, ac) > 0.98F)
        {
            isBetween = true;
        }

        return isBetween;
    }

    public static Vector3 Flat(this Vector3 vector)
    {
        return new Vector3(vector.x, 0, vector.z);
    }

    static float isLeft(Vector2 P0, Vector2 P1, Vector2 P2)
    {
        return ((P1.x - P0.x) * (P2.y - P0.y) - (P2.x - P0.x) * (P1.y - P0.y));
    }

    public static bool PointInRectangle2D(Vector2 X, Vector2 Y, Vector2 Z, Vector2 W, Vector2 P)
    {
        return (isLeft(X, Y, P) > 0 && isLeft(Y, Z, P) > 0 && isLeft(Z, W, P) > 0 && isLeft(W, X, P) > 0);
    }

    public static bool PointInRectangle3D(Vector3 X, Vector3 Y, Vector3 Z, Vector3 W, Vector3 P)
    {
        X = X.ToVector2_2DSpace();
        Y = Y.ToVector2_2DSpace();
        Z = Z.ToVector2_2DSpace();
        W = W.ToVector2_2DSpace();
        P = P.ToVector2_2DSpace();

        return PointInRectangle2D(X, Y, Z, W, P);
    }

    public static bool PointInRectangle3D(Square square, Vector3 P)
    {
        Vector3 X = square.Line1.A1;
        Vector3 Y = square.Line2.A1;
        Vector3 Z = square.Line2.A2;
        Vector3 W = square.Line1.A2;

        return PointInRectangle3D(X, Y, Z, W, P);
    }

    public struct Line
    {
        public Vector3 A1 { get; set; }
        public Vector3 A2 { get; set; }

        public Line(Vector3 a1, Vector3 a2)
        {
            a1.y = 0;
            a2.y = 0;
            A1 = a1;
            A2 = a2;
        }
    }

    public struct Square
    {
        public Line Line1 { get; set; }
        public Line Line2 { get; set; }

        public Square(Line line1, Line line2)
        {
            Line1 = line1;
            Line2 = line2;
        }
    }

    public static void DebugDrawSquare(VectorExstensions.Square square, Color color, float duration = 0)
    {
        if (duration == 0)
        {
            Debug.DrawLine(square.Line1.A1, square.Line1.A2, color);
            Debug.DrawLine(square.Line2.A1, square.Line2.A2, color);
            Debug.DrawLine(square.Line1.A1, square.Line2.A1, color);
            Debug.DrawLine(square.Line1.A2, square.Line2.A2, color);
        }
        else
        {
            Debug.DrawLine(square.Line1.A1, square.Line1.A2, color, duration);
            Debug.DrawLine(square.Line2.A1, square.Line2.A2, color, duration);
            Debug.DrawLine(square.Line1.A1, square.Line2.A1, color, duration);
            Debug.DrawLine(square.Line1.A2, square.Line2.A2, color, duration);
        }
    }

    public static Vector3 LineWithSquareIntersect(Line line, Square square, bool isFullCheck = false)
    {
        Vector3 intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line1.A1, square.Line1.A2);

        if (intersectPoint != Vector3.zero)
        {
            return intersectPoint;
        }

        intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line2.A1, square.Line2.A2);

        if (intersectPoint != Vector3.zero)
        {
            return intersectPoint;
        }

        if (isFullCheck)
        {
            intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line1.A1, square.Line2.A1);

            if (intersectPoint != Vector3.zero)
            {
                return intersectPoint;
            }

            intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line1.A2, square.Line2.A2);

            if (intersectPoint != Vector3.zero)
            {
                return intersectPoint;
            }
        }

        return Vector3.zero;
    }

    public static Vector3 LineWithSquareClosestPointIntersect(Line line, Square square)
    {
        Vector3 intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line1.A1, square.Line1.A2);

        if (intersectPoint != Vector3.zero)
        {
            return NearestPointOnLine(square.Line1, line.A1);
        }

        intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line2.A1, square.Line2.A2);

        if (intersectPoint != Vector3.zero)
        {
            return NearestPointOnLine(square.Line2, line.A1);
        }

        intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line1.A1, square.Line2.A1);

        if (intersectPoint != Vector3.zero)
        {
            return NearestPointOnLine(square.Line1.A1, square.Line2.A1 - square.Line1.A1, line.A1);
        }

        intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(line.A1, line.A2, square.Line1.A2, square.Line2.A2);

        if (intersectPoint != Vector3.zero)
        {
            return NearestPointOnLine(square.Line1.A2, square.Line2.A2 - square.Line1.A2, line.A1);
        }

        return Vector3.zero;
    }

    static Vector3 SquareWithSquareIntersect(Square square1, Square square2)
    {
        Vector3 intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line1.A1, square1.Line1.A2, square2.Line1.A1, square2.Line1.A2);

        if (intersectPoint != Vector3.zero)
        {
            return intersectPoint;
        }

        intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line1.A1, square1.Line1.A2, square2.Line2.A1, square2.Line2.A2);

        if (intersectPoint != Vector3.zero)
        {
            return intersectPoint;
        }

        intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line2.A1, square1.Line2.A2, square2.Line1.A1, square2.Line1.A2);

        if (intersectPoint != Vector3.zero)
        {
            return intersectPoint;
        }

        intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line2.A1, square1.Line2.A2, square2.Line2.A1, square2.Line2.A2);

        if (intersectPoint != Vector3.zero)
        {
            return intersectPoint;
        }
        return Vector3.zero;
    }

    public static Vector3 SquareWithSquareIntersect(Square square1, Square square2, bool isFullCheck)
    {
        Vector3 intersectPoint = SquareWithSquareIntersect(square1, square2);

        if (intersectPoint != Vector3.zero)
        {
            return intersectPoint;
        }

        if (isFullCheck)
        {
            intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line1.A1, square1.Line2.A1, square2.Line1.A1, square2.Line1.A2);

            if (intersectPoint != Vector3.zero)
            {
                return intersectPoint;
            }

            intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line1.A1, square1.Line2.A1, square2.Line2.A1, square2.Line2.A2);

            if (intersectPoint != Vector3.zero)
            {
                return intersectPoint;
            }

            intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line1.A2, square1.Line2.A2, square2.Line1.A1, square2.Line1.A2);

            if (intersectPoint != Vector3.zero)
            {
                return intersectPoint;
            }

            intersectPoint = VectorExstensions.GetIntersectionPointCoordinates3DSpaceWithCheckIntersection(square1.Line1.A2, square1.Line2.A2, square2.Line2.A1, square2.Line2.A2);

            if (intersectPoint != Vector3.zero)
            {
                return intersectPoint;
            }
        }

        return Vector3.zero;
    }


    public static Vector2 ToVector2(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.y);
    }

    public static Vector2 ToVector2_2DSpace(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.z);
    }

    public static Vector3 ToVector3_3DSpace(this Vector2 vector)
    {
        return new Vector3(vector.x, 0, vector.y);
    }

    public static bool IsEqual(this Vector3 lhs, Vector3 rhs)
    {
        return IsEqual(lhs, rhs, Mathf.Epsilon);
    }

    public static bool IsEqual(this Vector3 lhs, Vector3 rhs, float precision)
    {
        return Vector3.SqrMagnitude(lhs - rhs) < precision;
    }

    public static bool IsEqual(this Vector2 lhs, Vector2 rhs)
    {
        return IsEqual(lhs, rhs, Mathf.Epsilon);
    }

    public static bool IsEqual(this Vector2 lhs, Vector2 rhs, float precision)
    {
        return Vector3.SqrMagnitude(lhs - rhs) < precision;
    }

    public static int BetweenLineAndCircle(
    Vector3 circleCenter, float circleRadius,
    Vector3 point1, Vector3 point2,
    out Vector2 intersection1, out Vector2 intersection2)
    {
        Vector2 circleCenter2 = circleCenter.ToVector2_2DSpace();
        Vector2 point12 = point1.ToVector2_2DSpace();
        Vector2 point22 = point2.ToVector2_2DSpace();

        intersection1 = Vector3.zero;
        intersection2 = Vector3.zero;

        return BetweenLineAndCircle(
    circleCenter2, circleRadius,
    point12, point22,
    out intersection1, out intersection2);
    }

    public static int BetweenLineAndCircle(
    Vector2 circleCenter, float circleRadius,
    Vector2 point1, Vector2 point2,
    out Vector2 intersection1, out Vector2 intersection2)
    {
        float t;

        var dx = point2.x - point1.x;
        var dy = point2.y - point1.y;

        var a = dx * dx + dy * dy;
        var b = 2 * (dx * (point1.x - circleCenter.x) + dy * (point1.y - circleCenter.y));
        var c = (point1.x - circleCenter.x) * (point1.x - circleCenter.x) + (point1.y - circleCenter.y) * (point1.y - circleCenter.y) - circleRadius * circleRadius;

        var determinate = b * b - 4 * a * c;
        if ((a <= 0.0000001) || (determinate < -0.0000001))
        {
            // No real solutions.
            intersection1 = Vector2.zero;
            intersection2 = Vector2.zero;
            return 0;
        }
        if (determinate < 0.0000001 && determinate > -0.0000001)
        {
            // One solution.
            t = -b / (2 * a);
            intersection1 = new Vector2(point1.x + t * dx, point1.y + t * dy);
            intersection2 = Vector2.zero;
            return 1;
        }

        // Two solutions.
        t = (float)((-b + Mathf.Sqrt(determinate)) / (2 * a));
        intersection1 = new Vector2(point1.x + t * dx, point1.y + t * dy);
        t = (float)((-b - Mathf.Sqrt(determinate)) / (2 * a));
        intersection2 = new Vector2(point1.x + t * dx, point1.y + t * dy);

        return 2;
    }

    public static T FindClosestTarget<T>(IEnumerable<T> targets, Vector3 position) where T : Transform
    {
        return targets.OrderBy(item => (item.transform.position - position).sqrMagnitude).FirstOrDefault();
    }

    public static Vector3 FindClosestTarget(IEnumerable<Vector3> targets, Vector3 position)
    {
        return targets.OrderBy(targetPosition => (targetPosition - position).sqrMagnitude).FirstOrDefault();
    }
}