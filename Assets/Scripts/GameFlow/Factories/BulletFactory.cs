﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFactory : SingletonMonoBehaviour<BulletFactory>
{
    int poolSize = 100;

    [SerializeField] BulletPoolDictionary bulletPrefabs;

    Dictionary<BulletType, ObjectPool> bulletPools = new Dictionary<BulletType, ObjectPool>();

    protected override void Awake()
    {
        base.Awake();

        foreach (KeyValuePair<BulletType, DefaultBullet> pair in bulletPrefabs)
        {
            ObjectPool pool = PoolManager.Instance.PoolForObject(pair.Value.gameObject);
            pool.preInstantiateCount = poolSize;
            bulletPools.Add(pair.Key, pool);
        }
    }

    public GameObject GetBullet(BulletType bulletType)
    {
        GameObject weapon = bulletPools[bulletType].Pop();

        return weapon;
    }
}
