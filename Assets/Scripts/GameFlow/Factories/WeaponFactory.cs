﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFactory : SingletonMonoBehaviour<WeaponFactory>
{
    int poolSize = 5;

    [SerializeField] WeaponPoolDictionary weaponPrefabs;

    Dictionary<WeaponType, ObjectPool> weaponPools = new Dictionary<WeaponType, ObjectPool>();

    protected override void Awake()
    {
        base.Awake();

        foreach (KeyValuePair<WeaponType, Weapon> pair in weaponPrefabs)
        {
            ObjectPool pool = PoolManager.Instance.PoolForObject(pair.Value.gameObject);
            pool.preInstantiateCount = poolSize;
            weaponPools.Add(pair.Key, pool);
        }
    }

    public GameObject GetWeapon(WeaponType weaponType)
    {
        GameObject weapon = weaponPools[weaponType].Pop();

        return weapon;
    }
}