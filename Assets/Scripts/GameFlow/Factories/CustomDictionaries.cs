﻿using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class VfxPoolDictionary : SerializableDictionaryBase<VFXType, UnityEngine.ParticleSystem>
{
}

[System.Serializable]
public class WeaponPoolDictionary : SerializableDictionaryBase<WeaponType, Weapon>
{
}

[System.Serializable]
public class BulletPoolDictionary : SerializableDictionaryBase<BulletType, DefaultBullet>
{
}