﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : SingletonMonoBehaviour<EnemyFactory>
{
    private int POOL_SIZE = 200;

    [SerializeField] GameObject[] enemyPrefabs;

    List<ObjectPool> pools = new List<ObjectPool>();

    protected override void Awake()
    {
        base.Awake();

        for (int i = 0; i < enemyPrefabs.Length; i++)
        {
            var objectPool = PoolManager.Instance.PoolForObject(enemyPrefabs[i].gameObject);
            objectPool.preInstantiateCount = POOL_SIZE;
            objectPool.Parent = transform;

            pools.Add(objectPool);
        }
    }

    public GameObject GetRandomEnemy()
    {
        return pools[Random.Range(0, pools.Count)].Pop();
    }
}

