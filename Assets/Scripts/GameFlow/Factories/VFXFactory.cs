﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXFactory : SingletonMonoBehaviour<VFXFactory>
{
    int poolSize = 15;

    [SerializeField] VfxPoolDictionary vfxPrefabs;

    Dictionary<VFXType, ObjectPool> vfxPools = new Dictionary<VFXType, ObjectPool>();

    protected override void Awake()
    {
        base.Awake();

        foreach (KeyValuePair<VFXType, ParticleSystem> pair in vfxPrefabs)
        {
            ObjectPool pool = PoolManager.Instance.PoolForObject(pair.Value.gameObject);
            pool.preInstantiateCount = poolSize;
            vfxPools.Add(pair.Key, pool);
        }
    }

    public GameObject GetVFX(VFXType vfxType)
    {
        GameObject vfx = vfxPools[vfxType].Pop();

        return vfx;
    }
}