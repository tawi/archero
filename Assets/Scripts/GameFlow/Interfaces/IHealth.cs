﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealth 
{
    void TakeDamage(int amount);
    void TakeDamage(int amount, Vector3 forceDirection);
}
