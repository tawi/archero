﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishDoor : MonoBehaviour
{
    [SerializeField] private MeshRenderer doorEffect;

    private BoxCollider boxCollider;

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    private void OnEnable()
    {
        EnemySpawner.OnEnemiesEnded += EnemySpawner_OnEnemiesEnded;
    }

    private void OnDisable()
    {
        EnemySpawner.OnEnemiesEnded -= EnemySpawner_OnEnemiesEnded;
    }

    private void EnemySpawner_OnEnemiesEnded()
    {
        boxCollider.enabled = true;
        doorEffect.enabled = true;
    }
}