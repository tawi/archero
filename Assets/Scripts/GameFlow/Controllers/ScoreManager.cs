﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : SingletonMonoBehaviour<ScoreManager>
{
    private int coinsTotal;

    public int CoinsTotal
    {
        get => coinsTotal;
        set
        {
            coinsTotal = value;
            OnUpdateCoins(CoinsTotal);
        }
    }

    public event Action<int> OnUpdateCoins = delegate { };

    public void Subscribe(GameObject enemy)
    {
        enemy.GetComponent<EnemyHealth>().OnDeath += EnemyHealth_OnDeath;
    }

    private void EnemyHealth_OnDeath(HealthBase healthBase)
    {
        healthBase.OnDeath -= EnemyHealth_OnDeath;

        int coins = healthBase.GetComponent<EnemyBase>().CoinsAmount;

        CoinsTotal += coins;
    }
}