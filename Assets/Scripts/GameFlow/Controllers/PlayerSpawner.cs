﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : SingletonMonoBehaviour<PlayerSpawner>
{
    [SerializeField] PlayerController playerPrefab;
    [SerializeField] Transform spawnPosition;

    public PlayerController Player { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        Player = Instantiate(playerPrefab, spawnPosition.transform.position, Quaternion.identity);
    }

    private void Start()
    {
        CameraController.Instance.Target = Player.transform;
    }
}