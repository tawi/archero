﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class MapController : SingletonMonoBehaviour<MapController>
{
    [SerializeField] GameObject leftTopCornerCell;
    [SerializeField] GameObject rightBottomCornerCell;
    [SerializeField] Vector2 cellSize;
    [SerializeField] Transform[] obstacles;

    [SerializeField] Vector2Int leftTopMapCorner;
    private Vector2Int mapSize;
    private List<Vector2Int> forbiddenCells = new List<Vector2Int>();

    public Vector2Int MapSize { get => mapSize; }

    [Button]
    private void Test()
    {
        new GameObject("LeftTopCorner").transform.position = GetSpawnPositionRelativeCorner(leftTopMapCorner);
    }

    protected override void Awake()
    {
        base.Awake();

        leftTopMapCorner = new Vector2Int((int)(leftTopCornerCell.transform.position.x / cellSize.x), (int)(leftTopCornerCell.transform.position.z / cellSize.y));
        var leftBottomCell = new Vector2Int((int)(rightBottomCornerCell.transform.position.x / cellSize.x), (int)(rightBottomCornerCell.transform.position.z / cellSize.y));

        mapSize = new Vector2Int(leftBottomCell.x - leftTopMapCorner.x, leftTopMapCorner.y - leftBottomCell.y);

        for (int i = 0; i < obstacles?.Length; i++)
        {
            var cell = PositionToCell(obstacles[i].transform.position);
            forbiddenCells.Add(cell);
        }
    }

    public bool PositionIsValid(Vector3 sourcePosition)
    {
        var cell = PositionToCell(sourcePosition);

        return IsAllowedCell(cell);
    }

    public bool IsAllowedCell(Vector2Int cell)
    {
        return (!forbiddenCells.Contains(cell) &&
            (cell.x > 0 && cell.y > 0) &&
            (cell.x < mapSize.x && cell.y > 0) &&
            (cell.x > 0 && cell.y < mapSize.y) &&
            (cell.x < mapSize.x && cell.y < mapSize.y));
    }

    public Vector3 GetSpawnPositionRelativeCorner(Vector2Int cell)
    {
        return new Vector3(cellSize.x * (leftTopMapCorner.x + cell.x), 0, cellSize.y * (leftTopMapCorner.y - cell.y));
        // return new Vector3(cellSize.x * (0 + cell.x), 0, cellSize.y * (0 + cell.y));
    }

    public Vector2Int PositionToCell(Vector3 sourcePosition)
    {
        return new Vector2Int((int)(RoundValue(sourcePosition.x) / cellSize.x) - leftTopMapCorner.x, -(int)(RoundValue(sourcePosition.z) / cellSize.y) + leftTopMapCorner.y);
    }

    public Vector3 RoundPosition(Vector3 sourcePosition)
    {
        return new Vector3(RoundValue(sourcePosition.x), 0, RoundValue(sourcePosition.z));
    }

    public float RoundValue(float value)
    {
        float rounded = 0;

        float size = 0.5f;

        if (value > 0)
        {
            rounded = Mathf.Floor(value);

            float temp = value - rounded + Mathf.Floor(size);

            if (temp > size + size / 2)
            {
                rounded += size * 2;
            }
            else if (temp > size / 2)
            {
                rounded += size;
            }
        }
        else
        {
            rounded = Mathf.Ceil(value);

            float temp = Mathf.Abs(value - rounded + Mathf.Floor(size));

            if (temp > size + size / 2)
            {
                rounded -= size * 2;
            }
            else if (temp > size / 2)
            {
                rounded -= size;
            }
        }

        return rounded;
    }
}