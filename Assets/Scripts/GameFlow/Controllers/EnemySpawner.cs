﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;
using UnityEngine.AI;

public class EnemySpawner : SingletonMonoBehaviour<EnemySpawner>
{
    [SerializeField] float spawnPartLimit = 0.3f;
    [SerializeField] int spawnCount = 10;

    public List<Transform> Enemies { get; private set; } = new List<Transform>();

    public static event Action OnEnemiesEnded = delegate { };    

    private void Start()
    {
        for (int i = 0; i < spawnCount; i++)
        {
            Spawn();
        }
    }

    public void Spawn()
    {
        Vector2Int cell = default;

        while (true)
        {
            var mapSize = MapController.Instance.MapSize;
            int randomX = UnityEngine.Random.Range(1, mapSize.x);
            int randomY = UnityEngine.Random.Range(1, mapSize.y - Mathf.FloorToInt(spawnPartLimit * mapSize.y));

            cell = new Vector2Int(randomX, randomY);

            if (MapController.Instance.IsAllowedCell(cell))
            {
                break;
            }
        }

        Vector3 spawnPosition = MapController.Instance.GetSpawnPositionRelativeCorner(cell);

        var enemy = EnemyFactory.Instance.GetRandomEnemy();

        enemy.transform.position = spawnPosition;
        enemy.GetComponent<NavMeshAgent>().Warp(spawnPosition);
        enemy.GetComponent<HealthBase>().OnDeath += EnemyHealth_OnDeath;
        ScoreManager.Instance.Subscribe(enemy);

        Enemies.Add(enemy.transform);
    }

    private void EnemyHealth_OnDeath(HealthBase healthBase)
    {
        healthBase.OnDeath -= EnemyHealth_OnDeath;

        if (Enemies.Contains(healthBase.transform))
        {
            Enemies.Remove(healthBase.transform);
        }

        if (Enemies.Count == 0)
        {
            OnEnemiesEnded();
        }
    }
}