﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : SingletonMonoBehaviour<CameraController>
{
    const float DEFAULT_ASPECT = 0.5625F;

    [SerializeField] float minZ, maxZ;
    [SerializeField] float offset;

    public Transform Target { get; set; }

    protected override void Awake()
    {
        base.Awake();

        float aspect = Camera.main.aspect;

        float multiplier = DEFAULT_ASPECT / aspect;

        Camera.main.orthographicSize = Camera.main.orthographicSize * multiplier;
    }

    public void FixedUpdate()
    {
        if (Target)
        {
            Vector3 position = transform.position;

            float zPos = Target.position.z + offset;
            zPos = Mathf.Clamp(zPos, minZ, maxZ);
            position.z = zPos;

            transform.position = position;
        }
    }
}
