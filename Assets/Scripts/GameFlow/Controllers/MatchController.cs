﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchController : SingletonMonoBehaviour<MatchController>
{
    [SerializeField] float startDelay = 3f;

    public bool DoorIsOpened { get; private set; }

    public event Action OnStartGame = delegate { };
    public event Action<bool> OnLevelComplete = delegate { };

    protected override void Awake()
    {
        base.Awake();
        StartGame();
    }   

    private void StartGame()
    {
        StartCoroutine(StartTimer());
    }

    private IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(startDelay);
        OnStartGame();
    }   

    public void Finish(bool isSuccess)
    {
        OnLevelComplete(isSuccess);
    }
}
