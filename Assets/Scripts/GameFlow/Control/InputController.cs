﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : SingletonMonoBehaviour<InputController>
{
    [SerializeField] private Joystick joystick;

    public Vector2 GetInput()
    {
        float horizontal = 0;
        float vertical = 0;

        if (joystick.Horizontal != 0 || joystick.Vertical != 0)
        {
            horizontal = joystick.Horizontal;
            vertical = joystick.Vertical;
        }
        else
        {
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");
        }

        Vector2 input = new Vector2(horizontal, vertical);

        return input;
    }
}