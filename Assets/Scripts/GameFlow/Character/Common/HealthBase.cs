﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBase : MonoBehaviour, IHealth
{
    [SerializeField] int maxHp;

    protected int hp;

    public event Action<HealthBase> OnDeath = delegate { };

    private void OnEnable()
    {
        hp = maxHp;
    }

    public virtual void TakeDamage(int amount)
    {
        if (hp > 0)
        {
            hp -= amount;

            if (hp <= 0)
            {
                var vfx = VFXFactory.Instance.GetVFX(VFXType.DeathEffect);
                vfx.GetComponent<VFXBehaviour>().Play(transform.position);
                OnDeath(this);
            }
        }
    }

    public virtual void TakeDamage(int amount, Vector3 forceDirection)
    {
        GameObject vfx = VFXFactory.Instance.GetVFX(VFXType.BloodSplat);
        vfx.transform.rotation = Quaternion.LookRotation(-forceDirection);

        vfx.GetComponent<VFXBehaviour>().Play(transform.position);

        TakeDamage(amount);
    }
}
