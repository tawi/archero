﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBase : MonoBehaviour
{
    [SerializeField] float moveSpeed;

    private CharacterController characterController;

    protected virtual bool CanMove { get; set; }
    public bool IsMoving { get; private set; }
    public Vector3 Target { get; protected set; }
    public IHealth health { get; private set; }

    protected virtual void Awake()
    {
        characterController = GetComponent<CharacterController>();
        health = GetComponent<IHealth>();
    }

    private void Start()
    {
        MatchController.Instance.OnStartGame += MatchController_OnStartGame;
    }

    protected virtual void FixedUpdate()
    {
        if (!CanMove)
        {
            return;
        }

        Vector2 input = GetInput();
        IsMoving = input.x != 0 || input.y != 0;

        if (IsMoving)
        {
            Vector3 moveVector = new Vector3(input.x, 0, input.y).normalized * moveSpeed * Time.fixedDeltaTime;
            Move(moveVector);

            transform.rotation = Quaternion.LookRotation(moveVector);
        }
        else
        {
            ProcessTarget();
        }
    }

    protected virtual Vector2 GetInput()
    {
        return default;
    }

    protected virtual void Move(Vector3 moveVector)
    {
        characterController.Move(moveVector);
    }

    protected virtual void ProcessTarget()
    {
    }

    private void MatchController_OnStartGame()
    {
        CanMove = true;
    }
}