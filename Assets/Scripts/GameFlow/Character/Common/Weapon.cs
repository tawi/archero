﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private float fireRate;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private int damageAmount;
    [SerializeField] private WeaponType weaponType;
    [SerializeField] private BulletType bulletType;

    public float FireRate { get => fireRate; }
    public float BulletSpeed { get => bulletSpeed; }
    public int DamageAmount { get => damageAmount; }
    public WeaponType WeaponType { get => weaponType; }
    public BulletType BulletType { get => bulletType; }
}