﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXBehaviour : MonoBehaviour
{
    private ParticleSystem vfx;
    private float destroyTime;

    private bool ShouldDestroy
    {
        get
        {
            return destroyTime != 0 && Time.time > destroyTime;
        }
    }

    private void Awake()
    {
        vfx = GetComponent<ParticleSystem>();
    }

    private void OnDisable()
    {
        if (vfx != null)
        {
            vfx.Stop();
            destroyTime = 0;
        }
    }

    private void LateUpdate()
    {
        if (ShouldDestroy)
        {
            gameObject.ReturnToPool();
        }
    }

    public void Play(Vector3 vfxPosition)
    {
        vfx.Play();
        destroyTime = Time.time + vfx.main.duration;

        transform.position = vfxPosition;
    }

    public void PlayOneShot(Vector3 vfxPosition)
    {
        Play(vfxPosition);

        destroyTime = Time.time + vfx.main.startLifetime.constantMax;
    }

    public void Play(float duration, Vector3 vfxPosition)
    {
        vfx.Play();
        destroyTime = Time.time + duration;
        transform.position = vfxPosition;
    }
}