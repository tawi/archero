﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultBullet : MonoBehaviour
{
    protected float speed;
    protected int damageAmount;
    protected LayerMask layerMask;

    protected virtual void FixedUpdate()
    {
        RaycastHit raycastHit;

        float movementStep = speed * Time.fixedDeltaTime;
        Physics.Raycast(transform.position, transform.forward, out raycastHit, movementStep, layerMask);

        CheckHit(raycastHit, transform.forward, movementStep);
    }

    protected void CheckHit(RaycastHit raycastHit, Vector3 movingDirection, float movementStep)
    {
        if (raycastHit.collider != null)
        {
            IHealth health = raycastHit.transform.GetComponent<IHealth>();

            if (health != null)
            {
                health.TakeDamage(damageAmount, transform.forward);
            }

            gameObject.ReturnToPool();
        }
        else
        {
            transform.position += movingDirection * movementStep;
        }
    }

    public void Initialize(float speed, int damageAmount, LayerMask layerMask)
    {
        this.speed = speed;
        this.damageAmount = damageAmount;
        this.layerMask = layerMask;
    }

    public virtual void Launch(Vector3 sourcePosition, Vector3 direction)
    {
        transform.position = sourcePosition;
        transform.rotation = Quaternion.LookRotation(direction);
    }
}
