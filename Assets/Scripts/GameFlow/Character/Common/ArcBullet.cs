﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcBullet : DefaultBullet
{
    float yOffset = .5f;

    private Vector3 middlePoint;
    private Vector3 targetDirection;
    private Vector3 currentDirection;
    private Vector3 targetPosition;
    private Vector3 sourcePosition;
    private float lerpSpeed = 2f;
    private bool isUpDirection;
    private float targetDistance;

    protected override void FixedUpdate()
    {
        RaycastHit raycastHit;

        float movementStep = speed * Time.fixedDeltaTime;

        currentDirection = Vector3.Lerp(currentDirection, targetDirection, lerpSpeed * Time.fixedDeltaTime);

        Physics.Raycast(transform.position, currentDirection, out raycastHit, movementStep, layerMask);

        transform.rotation = Quaternion.LookRotation(currentDirection);

        if (isUpDirection)
        {
            float distance = Vector3.Distance(sourcePosition, transform.position.Flat());

            if (distance >= targetDistance * 0.3f)
            {
                isUpDirection = false;
                targetDirection = (targetPosition - middlePoint).normalized;
            }
        }
        else
        {
            targetDirection = (targetPosition - transform.position).normalized;
        }

        CheckHit(raycastHit, currentDirection, movementStep);
    }

    public override void Launch(Vector3 sourcePosition, Vector3 targetPoint)
    {
        isUpDirection = true;
        this.sourcePosition = sourcePosition;
        targetDistance = Vector3.Distance(sourcePosition, targetPoint);
        targetPosition = targetPoint;
        transform.position = sourcePosition + new Vector3(0, 0.2f);

        if (targetDistance > 1.5f)
        {
            middlePoint = (sourcePosition + targetPoint) / 2 + new Vector3(0, yOffset);
        }
        else
        {
            middlePoint = (sourcePosition + targetPoint) / 2;
        }

        targetDirection = (middlePoint - sourcePosition).normalized;
        currentDirection = targetDirection;
    }
}
