﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterWeapon : MonoBehaviour
{
    [SerializeField] protected Transform shootPoint;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private bool hasShootDistance = true;
    [SerializeField] private float maxShootDistance = 5f;

    protected CharacterBase character;
    private float shootTime;

    public bool InRangeOfShooting { get; private set; }

    protected bool CanShoot
    {
        get
        {
            bool isTooClose = true;
            InRangeOfShooting = true;

            if (hasShootDistance)
            {
                float distance = Vector3.Distance(character.Target, transform.position);

                isTooClose = distance < maxShootDistance;
                InRangeOfShooting = isTooClose;
            }

            return !character.IsMoving && Time.time > shootTime && character.Target != Vector3.zero && isTooClose;
        }
    }

    private void Awake()
    {
        character = GetComponent<CharacterBase>();
    }

    protected void Shoot(BulletType bulletType, float fireRate, float bulletSpeed, int damageAmount)
    {
        shootTime = Time.time + fireRate;

        Vector3 shootDirection = GetDirection();

        var bullet = BulletFactory.Instance.GetBullet(bulletType).GetComponent<DefaultBullet>();

        bullet.Initialize(bulletSpeed, damageAmount, layerMask);
        bullet.Launch(shootPoint.transform.position, shootDirection);
    }

    protected virtual Vector3 GetDirection()
    {
        Vector3 shootDirection = (character.Target - transform.position).normalized.Flat();

        return shootDirection;
    }
}