﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CharacterBase
{
    private const string FINISH_TAG = "Finish";
    private const string ENEMY_TAG = "Enemy";
    private const float HIT_TIME_RATE = 0.5F;

    private float hitTime;

    protected override Vector2 GetInput()
    {
        return InputController.Instance.GetInput();
    }   

    protected override void ProcessTarget()
    {
        base.ProcessTarget();

        if (EnemySpawner.Instance.Enemies.Count > 0)
        {
            Target = VectorExstensions.FindClosestTarget<Transform>(EnemySpawner.Instance.Enemies, transform.position).transform.position;

            Vector3 directionToTarget = (Target - transform.position).normalized.Flat();
            transform.rotation = Quaternion.LookRotation(directionToTarget);
        }
        else
        {
            Target = Vector3.zero;
        }
    }

    protected override void Move(Vector3 moveVector)
    {
        base.Move(moveVector);

        transform.position = transform.position.Flat();
    }

    private void CollisionHit(int damageAmount)
    {
        if (Time.time > hitTime)
        {
            hitTime = Time.time + HIT_TIME_RATE;       
            health.TakeDamage(damageAmount);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(FINISH_TAG))
        {
            MatchController.Instance.Finish(true);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.transform.CompareTag(ENEMY_TAG))
        {
            int damageAmount = hit.transform.GetComponent<EnemyBase>().CollisionHitAmount;
            CollisionHit(damageAmount);
        }
    }
}