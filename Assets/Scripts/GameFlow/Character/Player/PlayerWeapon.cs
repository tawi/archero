﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : CharacterWeapon
{
    [SerializeField] WeaponType defaultWeaponType;

    private Weapon currentWeapon;

    private void Start()
    {
        currentWeapon = WeaponFactory.Instance.GetWeapon(defaultWeaponType).GetComponent<Weapon>();
        currentWeapon.transform.parent = shootPoint;
        currentWeapon.transform.localPosition = Vector3.zero;
    }

    private void FixedUpdate()
    {
        if (CanShoot)
        {
            Shoot(currentWeapon.BulletType, currentWeapon.FireRate, currentWeapon.BulletSpeed, currentWeapon.DamageAmount);
        }
    }
}
