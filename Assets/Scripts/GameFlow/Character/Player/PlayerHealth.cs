﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : HealthBase
{
    public override void TakeDamage(int amount)
    {
        base.TakeDamage(amount);

        if (hp <= 0)
        {
            MatchController.Instance.Finish(false);
            gameObject.SetActive(false);
        }
    }
}