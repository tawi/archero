﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArcWeapon : EnemyWeapon
{
    protected override Vector3 GetDirection()
    {
        return character.Target;
    }
}
