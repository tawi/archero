﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : HealthBase
{
    public override void TakeDamage(int amount)
    {
        base.TakeDamage(amount);

        if (hp <= 0)
        {
            gameObject.ReturnToPool();
        }
    }
}
