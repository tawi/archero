﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBase : CharacterBase
{
    private const string PLAYER_TAG = "Player";

    [SerializeField] int collisionHitAmount = 1;
    [SerializeField] int coinsAmount = 10;
    [SerializeField] protected float updateMovementTimeRate = 2f;
    [SerializeField] protected float maxMovementDistance = 1f;
    [SerializeField] EnemyType enemyType;

    private NavMeshAgent navMeshAgent;
    private bool achievedTarget;
    private CharacterWeapon characterWeapon;
    protected Vector3 targetPosition;
    protected float updateMovementTime;

    public int CoinsAmount { get => coinsAmount; }
    public int CollisionHitAmount { get => collisionHitAmount; }

    protected override void Awake()
    {
        base.Awake();

        navMeshAgent = GetComponent<NavMeshAgent>();
        characterWeapon = GetComponent<CharacterWeapon>();
    }
    //    navMeshAgent.updatePosition = false;
    //    navMeshAgent.updateRotation = false;
    //}

    //protected override void FixedUpdate()
    //{
    //    base.FixedUpdate();

    //    navMeshAgent.nextPosition = transform.position;
    //}

    protected override Vector2 GetInput()
    {
        if (targetPosition == Vector3.zero)
            return default;

        float distanceToOldTarget = Vector3.Distance(navMeshAgent.pathEndPosition, targetPosition);

        if (distanceToOldTarget > 0.5f)
        {
            achievedTarget = false;
            navMeshAgent.SetDestination(targetPosition);
        }
        else if (navMeshAgent.hasPath && !achievedTarget)
        {
            float distanceToTarget = Vector3.Distance(navMeshAgent.pathEndPosition, transform.position);

            if (distanceToTarget > 0.3f)
            {
                Vector3 localTarget = navMeshAgent.pathEndPosition == navMeshAgent.steeringTarget ? navMeshAgent.pathEndPosition.Flat() : navMeshAgent.steeringTarget;
                Vector3 directionToSteeringTarget = (localTarget - transform.position).normalized;

                Vector2 input = new Vector2(directionToSteeringTarget.x, directionToSteeringTarget.z);

                return input;
            }
            else
            {
                achievedTarget = true;
            }
        }

        return default;
    }

    protected override void Move(Vector3 moveVector)
    {
        base.Move(moveVector);
    }

    protected override void ProcessTarget()
    {
        base.ProcessTarget();

        Target = PlayerSpawner.Instance.Player.transform.position;

        if (characterWeapon != null && characterWeapon.InRangeOfShooting)
        {
            Vector3 directionToTarget = (Target - transform.position).normalized.Flat();
            transform.rotation = Quaternion.LookRotation(directionToTarget);
        }
    }
}