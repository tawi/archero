﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyGround : EnemyBase
{
    [SerializeField] private float minDistanceToPlayerForMovement = 4f;

    protected override Vector2 GetInput()
    {
        var input = base.GetInput();

        var player = PlayerSpawner.Instance.Player;

        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance < minDistanceToPlayerForMovement && Time.time > updateMovementTime)
        {
            updateMovementTime = Time.time + updateMovementTimeRate;

            Vector3 randomPosition = default;

            while (true)
            {
                int randomAngle = UnityEngine.Random.Range(0, 360);

                randomPosition = transform.position + Quaternion.Euler(0, randomAngle, 0) * transform.forward * maxMovementDistance;

                bool hasTargetPoint = MapController.Instance.PositionIsValid(randomPosition);

                if (hasTargetPoint)
                {
                    randomPosition = MapController.Instance.RoundPosition(randomPosition);
                    break;
                }
            }

            targetPosition = randomPosition;
        }

        return input;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(targetPosition, 0.5f);
    }
}
