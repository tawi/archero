﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : CharacterWeapon
{
    [SerializeField] private float fireRate;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private int damageAmount;
    [SerializeField] private BulletType bulletType;


    private void FixedUpdate()
    {
        if (CanShoot)
        {
            Shoot(bulletType, fireRate, bulletSpeed, damageAmount);
        }
    }
}
