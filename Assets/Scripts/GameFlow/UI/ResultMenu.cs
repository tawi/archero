﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResultMenu : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI resultText;

    public void Initialize(bool isSuccess)
    {
        string text = isSuccess ? "SUCCESS!" : "FAIL!";
        resultText.SetText(text);
    }
}
