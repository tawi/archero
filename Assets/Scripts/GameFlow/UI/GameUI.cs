﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameUI : SingletonMonoBehaviour<GameUI>
{
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private PauseMenu pauseMenu;
    [SerializeField] private ResultMenu resultMenu;

    private void Start()
    {
        ScoreManager.Instance.OnUpdateCoins += ScoreManager_OnUpdateCoins;
        MatchController.Instance.OnLevelComplete += MatchController_OnLevelComplete;
    }

    public void PauseButton_OnClick()
    {
        pauseMenu.gameObject.SetActive(true);
    }

    private void ScoreManager_OnUpdateCoins(int value)
    {
        coinsText.SetText(value.ToString());
    }

    private void MatchController_OnLevelComplete(bool isSuccess)
    {
        resultMenu.gameObject.SetActive(true);
        resultMenu.Initialize(isSuccess);
    }
}