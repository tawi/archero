﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private void OnEnable()
    {
        Time.timeScale = 0f;
    }

    public void ResumeButton_OnResumeClick()
    {
        Time.timeScale = 1f;
        gameObject.SetActive(false);
    }
}
